package models

import (
	"errors"
	"strconv"
	"time"
)

var (
	UserList map[string]*User
)

func init() {
	UserList = make(map[string]*User)
	u := User{"user_11111", "astaxie", "11111", Profile{"male", 20, "Singapore", "astaxie@gmail.com"}}
	UserList["user_11111"] = &u
}

type User struct {
	Id       string
	Username string
	Password string
	Profile  Profile
}

type Profile struct {
	Gender  string
	Age     int
	Address string
	Email   string
}

type FriendsRequest struct {
	Id      string   `json:"id"`
	Friends []string `json:"friends" valid:"Required"`
}

type FriendsResponse struct {
	Success bool `json:"success"`
}

type FriendRetrieveRequest struct {
	Email string `json:"email" valid:"Required"`
}

type FriendRetrieveResponse struct {
	Success bool     `json:"success"`
	Friends []string `json:"friends"`
	Count   int      `json:"count"`
}

type FriendRetrieveCommonRequest struct {
	Friends []string `json:"friends" valid:"Required"`
}

type FriendSubscribeRequest struct {
	Requestor string `json:"requestor" valid:"Required"`
	Target    string `json:"target" valid:"Required"`
}

type FriendRetrieveAllRequest struct {
	Sender string `json:"sender" valid:"Required"`
	Text   string `json:"text" valid:"Required"`
}

type FriendRetrieveAllResponse struct {
	Success    bool     `json:"success"`
	Recipients []string `json:"recipients"`
}

func AddUser(u User) string {
	u.Id = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	UserList[u.Id] = &u
	return u.Id
}

func GetUser(uid string) (u *User, err error) {
	if u, ok := UserList[uid]; ok {
		return u, nil
	}
	return nil, errors.New("User not exists")
}

func GetAllUsers() map[string]*User {
	return UserList
}

func UpdateUser(uid string, uu *User) (a *User, err error) {
	if u, ok := UserList[uid]; ok {
		if uu.Username != "" {
			u.Username = uu.Username
		}
		if uu.Password != "" {
			u.Password = uu.Password
		}
		if uu.Profile.Age != 0 {
			u.Profile.Age = uu.Profile.Age
		}
		if uu.Profile.Address != "" {
			u.Profile.Address = uu.Profile.Address
		}
		if uu.Profile.Gender != "" {
			u.Profile.Gender = uu.Profile.Gender
		}
		if uu.Profile.Email != "" {
			u.Profile.Email = uu.Profile.Email
		}
		return u, nil
	}
	return nil, errors.New("User Not Exist")
}

func Login(username, password string) bool {
	for _, u := range UserList {
		if u.Username == username && u.Password == password {
			return true
		}
	}
	return false
}

func DeleteUser(uid string) {
	delete(UserList, uid)
}

func AddFriend(u FriendsRequest) (fresponse FriendsResponse) {
	u.Id = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	u.Friends = []string{"andy@example.com", "john@example.com"}
	fresponse.Success = true
	return fresponse
}

func RetrieveFriend(u FriendRetrieveRequest) (fresponse FriendRetrieveResponse) {
	u.Email = "andy@example.com"
	if u.Email != "" {
		fresponse.Friends = []string{"john@example.com"}
		if len(fresponse.Friends) > 0 {
			fresponse.Success = true
			fresponse.Count = len(fresponse.Friends)
		}
	}

	return fresponse
}

func RetrieveCommonFriend(u FriendRetrieveCommonRequest) (fresponse FriendRetrieveResponse) {
	u.Friends = []string{"andy@example.com", "john@example.com"}
	if len(u.Friends) > 0 {
		fresponse.Friends = []string{"common@example.com"}
		if len(fresponse.Friends) > 0 {
			fresponse.Success = true
			fresponse.Count = len(fresponse.Friends)
		}
	}

	return fresponse
}

func SubscribeFriend(u FriendSubscribeRequest) (fresponse FriendsResponse) {
	u.Requestor = "lisa@example.com"
	u.Target = "john@example.com"
	if u.Requestor != "" && u.Target != "" && u.Requestor != u.Target {
		fresponse.Success = true

	}

	return fresponse
}

func BlockFriend(u FriendSubscribeRequest) (fresponse FriendsResponse) {
	u.Requestor = "lisa@example.com"
	u.Target = "john@example.com"
	if u.Requestor != "" && u.Target != "" && u.Requestor != u.Target {
		fresponse.Success = true

	}

	return fresponse
}

func RetrieveAllFriend(u FriendRetrieveAllRequest) (fresponse FriendRetrieveAllResponse) {
	u.Sender = "john@example.com"
	u.Text = "Hello World! kate@example.com"
	if len(fresponse.Recipients) > 0 {
		fresponse.Recipients = []string{"lisa@example.com", "kate@example.com"}
		if len(fresponse.Recipients) > 0 {
			fresponse.Success = true
		}
	}

	return fresponse
}
