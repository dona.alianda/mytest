package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["mytest/controllers:ObjectController"] = append(beego.GlobalControllerRouter["mytest/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:ObjectController"] = append(beego.GlobalControllerRouter["mytest/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:ObjectController"] = append(beego.GlobalControllerRouter["mytest/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:ObjectController"] = append(beego.GlobalControllerRouter["mytest/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"put"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:ObjectController"] = append(beego.GlobalControllerRouter["mytest/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"delete"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/friend/add`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "Retrieve",
			Router: `/friend/retrieve`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "CommonRetrieve",
			Router: `/friend/common-retrieve`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "SubscribeFriend",
			Router: `/friend/subscribe`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "BlockFriend",
			Router: `/friend/subscribe`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["mytest/controllers:UserController"] = append(beego.GlobalControllerRouter["mytest/controllers:UserController"],
		beego.ControllerComments{
			Method: "RetrieveAll",
			Router: `/friend/all-etrieve`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

}
