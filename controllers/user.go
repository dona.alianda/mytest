package controllers

import (
	"encoding/json"
	"mytest/models"

	"github.com/astaxie/beego"
)

// Operations about Users
type UserController struct {
	beego.Controller
}

// @Title AddFriends
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/add [post]
func (u *UserController) Post() {
	var friends models.FriendsRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.AddFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}

// @Title Retrieve
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/retrieve [post]
func (u *UserController) Retrieve() {
	var friends models.FriendRetrieveRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.RetrieveFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}

// @Title CommonRetrieve
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/common-retrieve [post]
func (u *UserController) CommonRetrieve() {
	var friends models.FriendRetrieveCommonRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.RetrieveCommonFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}

// @Title SubscribeFriend
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/subscribe [post]
func (u *UserController) SubscribeFriend() {
	var friends models.FriendSubscribeRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.SubscribeFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}

// @Title BlockFriend
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/subscribe [post]
func (u *UserController) BlockFriend() {
	var friends models.FriendSubscribeRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.BlockFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}

// @Title RetrieveAll
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router /friend/all-etrieve [post]
func (u *UserController) RetrieveAll() {
	var friends models.FriendRetrieveAllRequest
	json.Unmarshal(u.Ctx.Input.RequestBody, &friends)
	resp := models.RetrieveAllFriend(friends)
	u.Data["json"] = resp
	u.ServeJSON()
}
